let count = 1;
let gradesSum = 0;
let average = 0;

function return_grade(){
    let grade = document.getElementById("grade-input");
    gradeValue = grade.value;
    if (((gradeValue).length) == 0){
        window.alert("Por favor, insira uma nota");
    } else if((gradeValue > 10) || (gradeValue < 0) || isNaN(gradeValue)) {
        window.alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    } else {
        let textarea = document.querySelector("textarea");
        textarea.append("A nota " + count + " foi " + gradeValue + "\n");
        gradeFloatValue = parseFloat(gradeValue);
        gradesSum = gradesSum + gradeFloatValue;
        count++;
    }
}


function return_average(){
    let average = gradesSum/(count - 1);
    let p = document.createElement("p");
    let text = "A média das notas é: " + average.toFixed(2);
    p.innerText = text;
    let form = document.querySelector(".form");
    form.append(p);
}

let btn = document.querySelector("#add-grade-btn");
btn.addEventListener("click", return_grade);

let average_btn = document.querySelector("#calculate-media-btn");
average_btn.addEventListener("click", return_average);



